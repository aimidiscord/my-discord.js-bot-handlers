if (config.environment !== 'production') require('dotenv').config();
// Import necessary modules

const {
    Client
} = require('discord.js');

// Initiate Classes
const bot = new Client({
    // No mentions are allowed by default
    disableMentions: 'all',
    // Only Users should be pinged (No Roles or @Everyone)
    allowedMentions: {
        parse: ['users']
    },
    // Presence Data for Login
    presence: {
        status: 'idle',
        afk: false,
        activity: {
            type: 'PLAYING',
            name: 'Hello World!'
        }
    }
});

require('./src/Functions/loadHandlers.js')(bot).then(() => {
    bot.login(process.env.token);
});