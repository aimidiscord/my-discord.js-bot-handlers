### Pretty Self Explanatory, This Repository has a base-handler for my discord bots so that i can get working asap, Feel free to fork it.

## Installation

> Clone the repository

```
git clone https://gitlab.com/aimidiscord/my-discord.js-bot-handlers.git
mv my-discord.js-bot-handlers (yourprojectname)
cd (yourprojectname)
```

> Install the default node modules
```
npm i
```

## Setup

> .env

Rename `sample.env` to `.env`. Put your token after `token=`.

> config.json

Put your id and prefix in the `config.json` file.

## Coding Part

The command structure can be found in `src/dumps/Command-Structure.md`. The First Parameter in events is the client itself, while the command paramters are client, message, messagearguments.