const {
    readdirSync
} = require('fs');
const {
    join
} = require('path');

const loadHandlers = async (bot) => {
    const HandlerFiles = readdirSync(join(__dirname, '../Handlers')).filter(file => file.endsWith('.js'));
    for (let i = 0; i < HandlerFiles.length; i++) {
        await require(join(__dirname, '../Handlers', HandlerFiles[i]))(bot);
    }
}

exports = loadHandlers;