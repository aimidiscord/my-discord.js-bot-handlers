# Structure of a command

## Basic Data

> _name_

The Name of the command.

__Type__: *String*

> _description_

The Description of the command and what it does.

__Type__: *String*

> _usage_

The Usage of the way a command must be used.

It only includes the part after the command. Don't include the Prefix and CommandName in it.

__Type__: *String*

> _guildOnly_

Whether or not to allow usage of this command in dms.
If set to true, Command can only be used in Servers.

__Type__: *Boolean*

> _permissions_

The permissions required by the author of the cmd.

__Type__: *String* or *Array*

> _botPermissions_

The permissions required by the bot.

__Type__: *String* or *Array*

> _aliases_

The aliases of the command (Alternate triggers).

__Type__: *Array*

> _cooldown_

The time (in seconds) after which you can re-use a command.

__Type__: *number*, or `3 seconds` if falsy

> _execute_

The actual function running the command

__Type__: *Async Or Sync Function (message, args, bot)*

## Usage Syntax

> <>

Mention

> []

Optional Parameter

> ()

Required