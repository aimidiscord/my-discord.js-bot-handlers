const {
    version
} = require('discord.js');
const {
    join
} = require('path');

module.exports = {
    name: 'ready',
    execute: async (bot) => {
        console.log(`${bot.user.tag} Logged into Discord.\nDiscord.js Version: ${version}\nNode.Js Version: ${process.version}`);
        if (!bot.owner) bot.owner = require(join(__dirname, '../../config.json')).owner;
    }
}