const {
  Collection
} = require('discord.js')
const {
  join
} = require('path');
const {
  red
} = require('chalk');

module.exports = {
  name: 'message',
  execute: async (bot, message) => {
      if (message.author.bot) return;
      if (!bot.prefix) {
          bot.prefix = require(join(__dirname, '../../config.json')).prefix;
      };
      if (!message.content.startsWith(bot.prefix)) return;
      const args = message.content.slice(bot.prefix.length).trim().split(/ +/);
      const commandName = args.shift().toLowerCase();
      const command = bot.commands.get(commandName) ||
          bot.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

      if (!command) return;

      if (command.guildOnly && message.channel.type === 'dm') {
          return message.reply('I can\'t execute that command inside DMs!');
      }

      if (command.permissions) {
          const authorPerms = message.channel.permissionsFor(message.author);
          if (!authorPerms || !authorPerms.has(command.permissions)) {
              let listofperms = typeof command.permissions == 'object' ? command.permissions.map(a => `• \`${a}\``).join('\n') : `• ${command.permissions}`;
              return message.reply('You do not have the neccessary permissions to carry that task. Please request a moderator or an admin to do it: ' + `\n${listofperms}`);
          }
      }

      if (command.botPermissions) {
          const botPermissions = message.channel.permissionsFor(message.client.user);
          if (!botPermissions || !botPermissions.has(command.botPermissions)) {
              let listofperms = typeof command.botPermissions == 'object' ? command.botPermissions.map(a => `• \`${a}\``).join('\n') : `• ${command.botPermissions}`;
              return message.reply('The Bot does not have the neccessary permissions to carry that task. Please request a moderator or an administrator to provide me the neccessary permissions (channelwide/serverwide):' + `\n${listofperms}`);
          }
      }

      if (command.args && !args.length) {
          let reply = `You didn't provide any arguments, ${message.author}!`;

          if (command.usage) {
              reply += `\nThe proper usage would be: \`${prefix}${commandName} ${command.usage}\`\nYou can also check out what to do in detail via \`${prefix}help ${command.name}\` `;
          }

          return message.channel.send(reply);
      }

      let {
          cooldowns
      } = bot;

      if (!cooldowns) {
          bot.cooldowns = cooldowns = new Collection();
      }

      if (!cooldowns.has(command.name)) {
          cooldowns.set(command.name, new Collection());
      }

      const now = Date.now();
      const timestamps = cooldowns.get(command.name);
      let cooldownAmount = (command.cooldown || 3) * 1000;
      if (command.cooldown === 0 && cooldownAmount == 3000) cooldownAmount = 0;
      if (timestamps.has(message.author.id)) {
          const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

          if (now < expirationTime) {
              const timeLeft = (expirationTime - now) / 1000;
              return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
          }
      }

      timestamps.set(message.author.id, now);
      setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
      if (bot.owner === message.author.id) {
          timestamps.delete(message.author.id);
      };


      try {
          command.execute(bot, message, args);
      } catch (error) {
          console.log(red(error));
          message.reply('there was an error trying to execute that command!');
      }
  }
}