// Import Standard Modules
const {
  readdirSync
} = require('fs');
const {
  red,
  blue
} = require('chalk');
const {
  join
} = require('path');

// Make the export function
const Events = async (bot) => {
  // Read the Folder and filter all the non-javascript files
  let EventFiles = readdirSync(join(__dirname, '../Events/')).filter(file => file.endsWith('.js'));
  console.log(blue.italic(`Found ${EventFiles.length} Event(s)`));
  // Literate through the Files
  for (let i = 0; i < EventFiles.length; i++) {
      const EventFile = EventFiles[i];
      // Read Every File and add a listener for it
      const EventData = require(join(__dirname, '../Events', EventFile));
      bot.on(EventData.name, (...args) => EventData.execute(bot, ...args))
      console.log(red(`${i+1}. ${EventData.name} (${EventFile})`))
  };
}

module.exports = Events