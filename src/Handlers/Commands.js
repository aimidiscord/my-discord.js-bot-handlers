// Import Standard Modules
const {
  Collection
} = require('discord.js')
const {
  readdirSync
} = require('fs');
const {
  green,
  red,
  blue
} = require('chalk');
const {
  join
} = require('path');

// Import Dirent Function
const retrieveDirectories = require('../Functions/retrieveDirectories.js');

// Make the export function
const Commands = async (bot) => {
  bot.commands = new Collection();
  // Read the Folder 
  let CommandFolders = retrieveDirectories(join(__dirname, '../Commands/'));
  // Check for Ungrouped Commands
  const UnGroupedCommands = readdirSync(join(__dirname, '../Commands/')).filter(file => file.endsWith('.js'));
  if (UnGroupedCommands.length) {
      console.log(red.italic(`${UnGroupedCommands.length >= 2 ? `${UnGroupedCommands[0]} and ${UnGroupedCommands.lengh - 1} were ` : `${UnGroupedCommands[0]} was `} ignored, Please group them in a folder.`));
  };

  // Literate through the Folders
  for (let i = 0; i < CommandFolders.length; i++) {
      const CommandFolder = CommandFolders[i];
      // Read Every Folder and Assign it into bot's Cache
      const FolderCommands = readdirSync(join(__dirname, '../Commands/', CommandFolder)).filter(file => file.endsWith('.js'));
      // CommandCache[CommandFolder] = [];
      for (let ii = 0; ii < FolderCommands.length; ii++) {
          let command = require(join(__dirname, '../Commands', CommandFolder, FolderCommands[ii]));
          command.catergory = CommandFolder;
          command.file = FolderCommands[ii];
          command.id = `${i+1}.${ii+1}`
          // CommandCache[CommandFolder].push(command);
          bot.commands.set(command.name, command);
      };
  };

  // Log the Command Folders found!
  console.log(blue.italic('Found ' + bot.commands.size + ' Commands in ' + CommandFolders.length + ' Command Directories (Groups)'));
  for (let i = 0; i < CommandFolders.length; i++) {
      console.log(red(`${i+1}. ${CommandFolders[i]}`));
      const Commands = bot.commands.filter(command => command.catergory == CommandFolders[i]);
      let commandindex = 0;
      Commands.forEach(command => {
          commandindex++;
          console.log(green.bold(`${i+1}.${commandindex} `) + green(`${command.name} (${command.file})`))
      })
  }
}

module.exports = Commands