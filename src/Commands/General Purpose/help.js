// Import the MessageEmbed Class from Discord.JS which will be used to extend our help embed
const {
    MessageEmbed
} = require('discord.js');

// Make the help Embed Class
class generalHelp extends MessageEmbed {
    // The constructor is the `client` and `message`
    constructor(bot, message) {
        // Since MessageEmbed does not need any parameters, we can safely pass nothing into the super.
        super();
        // This is the Object which stores all of our commands and catergories. In order to access a given catergories' commands, you can use commands[catergory]
        this.commands = {};
        // Literate through all of our commands
        bot.commands.forEach((command) => {
            // If we do not have the catergory cached, We'll set it to an array with that command.
            if (!Object.keys(this.commands).includes(command.catergory)) {
                this.commands[command.catergory] = [command];
            } else { // Else We know that we already have a command from that catergory, so we will just *push* the command into the array
                this.commands[command.catergory].push(command);
            };
        });
        // Set the user as the author
        this.setAuthor(`${message.author.username}#${message.author.discriminator}`, `${message.author.displayAvatarURL({
      format: 'png',
      size: 512,
      dynamic: true
    })}`);
        // Set the bot's pfp
        this.setThumbnail(bot.user.displayAvatarURL({
            format: 'png',
            size: 512,
            dynamic: true
        }))
        // Footer
        this.setFooter('Just another Multipurpose Discord Bot | Made with ❤️ by Aimi', bot.user.displayAvatarURL({
            format: 'png',
            size: 512,
            dynamic: true
        }))
        // The Description
        this.setDescription(`Here's a list of all my commands. Feel free to use \`${bot.prefix}help [command]\` to get more info on the command.`)
        // Loop through all the Catergories
        for (let i = 0; i < Object.keys(this.commands).length; i++) {
            // Get the Current one
            const currentCatergory = Object.keys(this.commands)[i];
            // Map all the commands in the current catergory
            const commandString = this.commands[currentCatergory].map(command => `\`${command.name}\``).join(', ');
            // Add it as a *non-inline* field
            this.addField(`• ${currentCatergory}`, `> ${commandString}`, false);
        };
        // Set the timestamp
        this.setTimestamp(Date.now() || new Date());
        // Set the color
        this.setColor('LUMINOUS_VIVID_PINK')
    }
}

module.exports = {
    name: 'help',
    description: 'Gives you an overview over the bot\'s commands.',
    usage: ' [command]',
    guildOnly: false,
    permissions: [],
    botPermissions: ['EMBED_LINKS', 'SEND_MESSAGES'],
    aliases: ['commands'],
    cooldown: 2,
    execute: async (bot, message, args) => {
        if (!args.length) return message.channel.send(new generalHelp(bot, message));
        const command = bot.commands.get(args.join(' ')) ||
            bot.commands.find(cmd => cmd.aliases && cmd.aliases.includes(args.join(' ')));
        let embed = new MessageEmbed()
            .setTitle(`Aha! Heres the file info for your command`)
            .setDescription(`${command.name}'s Info`)
            .setFooter(`Command ID: ${command.id}`)
            .setColor('ORANGE');
        embed.addField('Name', command.name, true);
        embed.addField('Description', command.description);
        embed.addField('Catergory', command.catergory, true);
        embed.addField('Aliases', command.aliases ? command.aliases.map(alias => `\`${alias}\``).join(', ') : 'I got no aliases baby', true);
        embed.addField('Cooldown', (command.cooldown || 3) + ' second(s)', true);
        embed.addField('Permissions Needed (for the author)', command.permissions.length ? command.permissions.map(a => `• \`${a}\``).join('\n') : 'No Permissions needed!', true);
        embed.addField('Permissions Needed (for the bot)', command.botPermissions.length ? command.botPermissions.map(a => `• \`${a}\``).join('\n') : 'No Permissions needed!', true);
        message.channel.send(embed);
    }
}