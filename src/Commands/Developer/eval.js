const clean = text => {
    if (typeof(text) === "string")
        return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
    else
        return text;
}

module.exports = {
    name: 'eval',
    description: 'Lets the developer evaluate code.',
    usage: '[code]',
    guildOnly: false,
    permissions: [],
    botPermissions: [],
    cooldown: 0,
    execute: async (bot, message, args) => {
        if (message.author.id !== bot.owner) return;
        if (!args.length) return message.channel.send('I apparently can not evaluate air!!!');

        try {
            const code = args.join(" ");
            let evaled = eval(code);

            if (typeof evaled !== "string")
                evaled = require("util").inspect(evaled);

            message.channel.send(clean(evaled), {
                code: "xl"
            });
        } catch (err) {
            message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
        }
    }
}